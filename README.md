# BambooHR Ruby Client

Client for BambooHR API. https://documentation.bamboohr.com/docs

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bamboohr-ruby', require: false
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install bamboohr-ruby

## Usage

### Configure your client

Configure the client, you can get the api key following the steps described in the docs https://documentation.bamboohr.com/docs#authentication

```ruby
require 'bamboohr'

client = BambooHR::Client.new('api_key', 'subdomain')

# (Optional) Set a specific Faraday connection, mainly for testing purposes
client = BambooHR::Client.new('api_key', 'subdomain', Faraday.new)
```

### Resources

Resources this client supports:

```
https://api.bamboohr.com/api/gateway.php/{subdomain}/v1/employees/directory
https://api.bamboohr.com/api/gateway.php/{subdomain}/v1/employees/{id}/
```

### Examples

#### Employees

```ruby
# Get employee directory
employees = client.employee_directory

# Get employee information (display name and work email)
id = 1234567890
employee = client.employee(id)

# Get employee information with custom fields
fields = ['displayName', 'workEmail', 'jobTitle', 'photoUrl']
employee = client.employee(id, fields)

# Employee model
employee.can_upload_photo # [Boolean] false
employee.department       # [String]  'Sales'
employee.display_name     # [String]  'Some User'
employee.division         # [String]  'Management'
employee.first_name       # [String]  'Some'
employee.id               # [Integer] 1234567890
employee.job_title        # [String]  'CEO'
employee.last_name        # [String]  'User'
employee.linked_in        # [String]  'https://linkedin.com/in/nivelat'
employee.location         # [String]  'Chile'
employee.mobile_phone     # [String]  '56976543212'
employee.photo_uploaded   # [Boolean] true
employee.photo_url        # [String]  'https://resources.bamboohr.com/images/photo_person_150x150.png'
employee.preferred_name   # [String]  'Max'
employee.pronouns         # [String]  'He/him'
employee.supervisor       # [String]  'Supervisor Name'
employee.work_email       # [String]  'some.user@company.com'
employee.work_phone       # [String]  '56226543212'
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
