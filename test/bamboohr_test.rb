require "test_helper"

class BambooHRTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::BambooHR::VERSION
  end

  def test_the_client_returns_a_bamboohr_client
    assert_kind_of ::BambooHR::Client, ::BambooHR.client('key', 'domain')
  end
end
