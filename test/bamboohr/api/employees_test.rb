require_relative '../../test_helper'
require 'bamboohr/models/employee'

class BambooHRAPIEmployeesTest < Minitest::Test
  def test_that_it_has_a_employee_directory_method
    assert_respond_to test_client, :employee_directory
  end

  def test_employee_directory_returns_employee_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('http:/api/gateway.php/domain/v1/employees/directory') do
      stub_employee_directory_single_result
    end
    
    cli = test_client(stubs)

    employees = cli.employee_directory

    assert_equal employees.count, 1

    employees.each do |employee|
      assert_kind_of BambooHR::Employee, employee
    end
  end

  def test_employee_directory_returns_empty_array
    stubs = Faraday::Adapter::Test::Stubs.new
    stubs.get('http:/api/gateway.php/domain/v1/employees/directory') do
      stub_employee_directory_empty
    end
    
    cli = test_client(stubs)
    assert_equal cli.employee_directory, []
  end

  private

  def stub_employee_directory_empty
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"employees":[]}'
    ]
  end
  
  def stub_employee_directory_single_result
    [
      200,
      { 'Content-Type': 'application/javascript' },
      '{"employees":[{"id":"240","displayName":"Test User","firstName":"Test","lastName":"User","preferredName":"Test","jobTitle":"Analyst","workPhone":null,"mobilePhone":"123456789","workEmail":"test.user@company.com","department":null,"location":"United States","division":"Operations","linkedIn":null,"pronouns":null,"workPhoneExtension":null,"supervisor":"Supervisor User","photoUploaded":false,"photoUrl":"https:\/\/resources.bamboohr.com\/images\/photo_person_150x150.png","canUploadPhoto":0}]}'
    ]
  end
end
