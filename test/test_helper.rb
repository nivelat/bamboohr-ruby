$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "bamboohr"

require "minitest/autorun"

def test_client(stubs = nil)
  ::BambooHR::Client.new('key', 'domain', test_conn(stubs))
end

def test_conn(stubs = nil)
  stubs ||= Faraday::Adapter::Test::Stubs.new

  Faraday.new do |connection|
    connection.adapter :test, stubs
    connection.request :json
    connection.request :url_encoded
    connection.response :json, content_type: /.*/
  end
end