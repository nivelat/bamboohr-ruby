require 'bamboohr/version'
require 'bamboohr/client'

module BambooHR
  class << self
    def client(api_key, subdomain)
      BambooHR::Client.new(api_key, subdomain)
    end
  end
end
