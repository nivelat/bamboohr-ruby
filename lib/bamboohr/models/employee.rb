require 'bamboohr/models/base'

module BambooHR
  class Employee < Base
    attr_reader :can_upload_photo,
                :department,
                :display_name,
                :division,
                :first_name,
                :id,
                :job_title,
                :last_name,
                :linked_in,
                :location,
                :mobile_phone,
                :photo_uploaded,
                :photo_url,
                :preferred_name,
                :pronouns,
                :supervisor,
                :work_email,
                :work_phone,
                :work_phone_extension
    
    def initialize(attributes)
      @can_upload_photo = attributes['canUploadPhoto']
      @department = attributes['department']
      @display_name = attributes['displayName']
      @division = attributes['division']
      @first_name = attributes['firstName']
      @id = attributes['id']
      @job_title = attributes['jobTitle']
      @last_name = attributes['lastName']
      @linked_in = attributes['linkedIn']
      @location = attributes['location']
      @mobile_phone = attributes['mobilePhone']
      @photo_uploaded = attributes['photoUploaded']
      @photo_url = attributes['photoUrl']
      @preferred_name = attributes['preferredName']
      @pronouns = attributes['pronouns']
      @supervisor = attributes['supervisor']
      @work_email = attributes['workEmail']
      @work_phone = attributes['workPhone']
      @work_phone_extension = attributes['workPhoneExtension']
    end
  end
end