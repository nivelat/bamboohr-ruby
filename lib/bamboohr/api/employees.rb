require 'bamboohr/models/employee'

module BambooHR
  module API
    module Employees
      # Get employee data by specifying a set of fields. This is suitable for
      # getting basic employee information, including current values for fields
      # that are part of a historical table, like job title, or compensation
      # information. See the fields endpoint for a list of possible fields.
      #
      # @param id [Integer] is an employee ID. The special employee ID of zero
      # (0) means to use the employee ID associated with the API key (if any).
      #
      # @return [BambooHR::Employee] Response from API.
      def employee(id, fields = ['displayName', 'workEmail'])
        path = "/employees/#{id}/"
        path += "?fields=#{fields.join(',')}" unless fields.empty?
        result = request(:get, path)

        BambooHR::Employee.new(result)
      end
      
      # Gets employee directory.
      #
      # @return [BambooHR::Employee[]] Response from API.    
      def employee_directory
        result = request(:get, '/employees/directory')
        result['employees'].map { |e| BambooHR::Employee.new(e) }
      end
    end
  end
end