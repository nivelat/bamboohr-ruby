require 'faraday'

require 'bamboohr/api/employees'

module BambooHR
  class Client
    include BambooHR::API::Employees

    def initialize(api_key, subdomain, conn = nil)
      @api_key = api_key
      @subdomain = subdomain
      @conn = conn
    end

    private

    def request(method, path, body = nil)
      client.public_send(method, build_path(path), body).body
    end

    def build_path(path)
      "/api/gateway.php/#{@subdomain}/v1#{path}"
    end

    def client
      @conn ||= ::Faraday.new('https://api.bamboohr.com') do |connection|
        connection.headers['Accept'] = 'application/json'

        connection.request :json
        connection.request :url_encoded
        connection.request :basic_auth, @api_key, 'x'

        connection.response :json
        connection.response :logger, nil, { headers: true, bodies: true }

        connection.adapter Faraday.default_adapter
      end
    end
  end
end